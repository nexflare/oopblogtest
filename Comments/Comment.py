import datetime as dt


class Comment():
    """
    Comments are simply a message, a timestamp, and the author.
    Comments can also have a reply, so we'll store what the parent comment was.
    """

    def __init__(self, message, author, replied_to=None):
        self._message = message
        self._author = author
        self._timestamp = dt.datetime.now()
        self.replied_to = replied_to

    def __del__(self):
        return f"{self} deleted!"

    @property
    def author(self):
        return self._author

    @property
    def message(self):
        return self._message

    def edit(self, message):
        """ if the author can edit then update the message otherwise show warning message"""
        self._message = message
        print("edited!")
        print(self.message)

    def __str__(self):
        if self.replied_to is not None:
            return f"{self._message} by {self._author} replied to {self.replied_to.message} by {self.replied_to.author.username}"  # noqa
        return f"{self._message} by {self._author}"
