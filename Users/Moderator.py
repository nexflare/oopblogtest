from .User import User


class Moderator(User):
    """
    Moderator is a User
    Moderators can only edit their own comments
    Moderators can delete any comments
    """

    def can_delete(self, comment):
        return True
