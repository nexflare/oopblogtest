from .Moderator import Moderator


class Admin(Moderator):
    """
    Admins are users that have the ability to add, edit or delete any comment.
    Admin is both a User and a Moderator
    Admins can create any comments
    Admins can edit any comments
    Admins can delete any comments
    """

    def can_edit(self, comment):
        return True

    def can_delete(self, comment):
        return True
