import datetime as dt
import sys

sys.path.append("..")

from Comments.Comment import Comment # noqa

"""
Abstract

Users come in 3 flavors, normal users, moderators, and admins.
Normal users can only create new comments, and edit the their own comments.
Moderators have the added ability to delete comments (to remove trolls),
while admins have the ability to edit or delete any comment.
Users can log in and out, and we track when they last logged in

Actions

- Users can be logged in and out.
- When logging in, set the last_logged_in_at timestamp.
- Do not modify this timestamp when logging out
- Users can only edit their own comments
- Users cannot delete any comments

Defaults values

- Should be marked as not logged in
- Should return None for the last logged in at property
"""


class User:
    def __init__(self, username="test_user"):
        self.__is_logged_in = False
        self.__last_logged_in = None
        self._username = username

    @property
    def username(self):
        return self._username

    @property
    def is_logged_in(self):
        return self.__is_logged_in

    @property
    def last_logged_in(self):
        return self.__last_logged_in

    @is_logged_in.setter
    def set_is_logged_in(self, is_logged_in):
        """
        is_logged_in -> Bool
        """
        self.__is_logged_in = is_logged_in
        self.__last_logged_in = dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")

    def logout(self):
        self.set_is_logged_in = False

    def login(self):
        self.set_is_logged_in = True

    def get_online_status(self):
        if self.is_logged_in is True:
            return f"You have logged in at {self.__last_logged_in}"
        else:
            return f"You have logged out at {self.__last_logged_in}"

    def new_comment(self, message, replied_to=None):
        return Comment(message, self, replied_to)

    def edit_comment(self, comment, new_message):
        if self.can_edit(comment):
            comment.edit(new_message)
            return "Cool! You can edit this one!"
        return "You cannot edit this comment!"

    def delete_comment(self, comment):
        if self.can_delete(comment):
            print(f"Deleting {comment} ...")
            try:
                del comment
            except TypeError:
                print("Comment already deleted!")
            return "deleted!"
        return "You cannot delete this comment!"

    def can_edit(self, comment):
        return comment.author == self

    def can_delete(self, comment):
        return False

    def toString(self):
        return self

    def __str__(self):
        return f"{self.username}"
