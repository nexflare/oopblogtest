from Users import User, Admin, Moderator

if __name__ == "__main__":
    user = User.User(username="iflare3g")
    admin = Admin.Admin(username="admin")
    moderator = Moderator.Moderator(username="moderator")

    admin_comment = admin.new_comment("Admin comment")
    user_comment = user.new_comment("First comment by API")
    comment_with_reply = user.new_comment("Reply", replied_to=user_comment)
    moderator_comment = moderator.new_comment("Moderator comment")

    print("Can edit tests...")
    # Test user can edit its comment
    print(user.can_edit(user_comment))  # expected True

    # Test admin can edit its comment
    print(admin.can_edit(admin_comment))  # expected True

    # Test user can edit an admin comment
    print(user.can_edit(admin_comment))  # expected False

    # Test admin can edit a user comment
    print(admin.can_edit(user_comment))  # expected True

    print("Can delete tests...")
    print(user.can_delete(user_comment))  # expected False
    print(user.can_delete(admin_comment))  # expected False
    print(user.can_delete(moderator_comment))  # expected False
    print(admin.can_delete(admin_comment))  # expected True
    print(admin.can_delete(user_comment))  # expected True
    print(admin.can_delete(moderator_comment))  # expected True
    print(moderator.can_delete(user_comment))  # expected True
    print(moderator.can_delete(admin_comment))  # expected True

    print("toString() tests...")
    # toString() tests
    print(user.toString())
    print(admin.toString())
    print(moderator.toString())

    print("Editing tests...")
    print("Test editing Should fail...")
    print(user.edit_comment(admin_comment, "edited by iuser"))  # expected False
    print(moderator.edit_comment(admin_comment, "edited by admin"))  # expected False

    print("Test editing Should be good...")
    print(user.edit_comment(user_comment, "edited by iuser"))  # expected True
    print(
        admin.edit_comment(user_comment, "edited by admin a user comment")
    )  # expected True
    print(
        admin.edit_comment(admin_comment, "edited by admin its comment")
    )  # expected True
    print(
        moderator.edit_comment(moderator_comment, "edited by moderator its comment")
    )  # expected True

    print("Test deleting...")
    print("Test deleting Should fail...")
    print(user.delete_comment(admin_comment))  # expected False

    print("Test deleting should be good")
    print(admin.delete_comment(user_comment))  # expected True
    print(moderator.delete_comment(user_comment))  # expected True
